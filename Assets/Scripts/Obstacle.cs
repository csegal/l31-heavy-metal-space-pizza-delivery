﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (Rigidbody2D))]
public class Obstacle : MonoBehaviour {

	[Range (-100, 100)]
	public float rotationSpeed = 0;

	private Rigidbody2D rb;
	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D>();
		rb.angularVelocity = rotationSpeed;
	}
}
