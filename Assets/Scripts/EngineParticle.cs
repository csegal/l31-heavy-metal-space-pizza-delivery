﻿using UnityEngine;
using System.Collections;

public class EngineParticle : MonoBehaviour {

	public ShipMotor sm;
	private ParticleSystem ps;

	// Use this for initialization
	void Start () {
		ps = GetComponent<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () {
		if ( sm.engineFiring ) {
			ps.Play();
		} else {
			ps.Stop();
		}
	}
}
