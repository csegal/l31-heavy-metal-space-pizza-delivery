﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GUIBar : MonoBehaviour {

	[Range(0,1)]
	public float lerpSpeed = 0.1f;

	private float fill = 1;
	public float Fill {
		get {
			return fill;
		}
		set {
			fill = value;
		}
	}

	private Image image;

	// Use this for initialization
	void Start () {
		image = GetComponent<Image>();
	}

	void Update () {
		image.fillAmount = Mathf.Lerp( image.fillAmount, fill, lerpSpeed );
	}
}
