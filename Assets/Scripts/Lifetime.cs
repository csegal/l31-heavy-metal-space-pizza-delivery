﻿using UnityEngine;
using System.Collections;

public class Lifetime : MonoBehaviour {

	public float lifetime = 1;
	
	// Use this for initialization
	void Start () {
		Invoke( "Die", lifetime );	
	}

	void Die() {
		Destroy( gameObject );
	}
}
