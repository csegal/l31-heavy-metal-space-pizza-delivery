﻿using UnityEngine;
using System.Collections;

public class Music : MonoBehaviour {

	public AudioClip[] clips;

	private int currentIndex = -1;

	private static Music _instance;

	private AudioSource source;

	// Use this for initialization
	void Start () {
		if ( _instance ) {
			Destroy( gameObject );
			return;
		}
		_instance = this;
		DontDestroyOnLoad( gameObject );

		source = GetComponent<AudioSource>();

		Shuffle ();
	}
	
	// Update is called once per frame
	void Update () {
		if ( ! source.isPlaying ) {
			currentIndex ++;

			if ( currentIndex >= clips.Length ) {
				currentIndex = 0;
			}

			source.clip = clips[currentIndex];
			source.Play();
		}
	}

	public void Shuffle()  {
		int n = clips.Length;
		while (n > 1) {
			n--;
			int k = Random.Range(0, n + 1);
			AudioClip value = clips[k];
			clips[k] = clips[n];
			clips[n] = value;
		}
	}
}
