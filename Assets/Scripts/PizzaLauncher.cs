﻿using UnityEngine;
using System.Collections;

public class PizzaLauncher : MonoBehaviour {

	public string buttonName;

	[Range (0, 10)]
	public int numPizzas = 1;
	[Range (0, 3)]
	public float reloadSpeed = 0.2f;
	[Range (0, 50)]
	public float projectileSpeed = 5;

	private float lastShot = 0;

	public PizzaProjectile pizzaPrefab;

	public AudioClip firePizzaSound;

	private Transform tf;
	private Rigidbody2D rb;

	void Start () {
		tf = GetComponent<Transform>();
		rb = tf.root.GetComponent<Rigidbody2D>();

		tf.root.GetComponent<PizzaDeliveryDieOnCollision>().shipCrash += Crashed;
	}

	void Update () {
		if ( Input.GetButtonDown( buttonName ) && ( Time.time - lastShot ) > reloadSpeed && numPizzas > 0  ) {

			PizzaProjectile pizza = Instantiate( pizzaPrefab, transform.position, Quaternion.identity ) as PizzaProjectile;

			Vector2 velocity = new Vector2( tf.up.x, tf.up.y ).normalized * projectileSpeed;
			pizza.GetComponent<Rigidbody2D>().velocity = velocity + rb.velocity;

			lastShot = Time.time;

			if ( firePizzaSound != null ) {
				AudioSource.PlayClipAtPoint( firePizzaSound, tf.position );
			}

			TimeManager.Instance.StartTimer();

			numPizzas--;
		}
	}

	void Crashed( Collision2D col ) {
		numPizzas = 0;
	}
}
