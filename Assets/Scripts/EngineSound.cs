﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (ShipMotor))]
[RequireComponent (typeof (AudioSource))]
public class EngineSound : MonoBehaviour {
	private AudioSource source;
	private ShipMotor motor;
	// Use this for initialization
	void Start () {
		source = GetComponent<AudioSource>();
		motor = GetComponent<ShipMotor>();
	}
	
	// Update is called once per frame
	void Update () {
		if( motor.engineFiring && ! source.isPlaying ) {
			source.Play();
		} else if ( ! motor.engineFiring ) {
			source.Stop();
		}
	}
}
